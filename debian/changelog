octavia-tempest-plugin (2.9.0-1) experimental; urgency=medium

  * New upstream release.
  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Mar 2025 15:05:30 +0100

octavia-tempest-plugin (2.8.0-4) unstable; urgency=medium

  * Add golang-any as build-depends and build test_server.bin.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jan 2025 14:01:35 +0100

octavia-tempest-plugin (2.8.0-3) unstable; urgency=medium

  * Add package-all-files.patch.
  * Add allow-_enable_ipv6_nic_webserver-ip-addr-add-to-fail.patch.
  * Add python3-h2 as runtime depends, needed for a TLSWithBarbicanTest test.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jan 2025 13:46:55 +0100

octavia-tempest-plugin (2.8.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090454).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 12:18:35 +0100

octavia-tempest-plugin (2.8.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Sep 2024 16:46:27 +0200

octavia-tempest-plugin (2.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 23:18:55 +0200

octavia-tempest-plugin (2.7.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 Aug 2024 09:37:08 +0200

octavia-tempest-plugin (2.6.0-1) unstable; urgency=medium

  * Uploading to unstable (Closes: #1067063).
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 25 Mar 2024 11:30:38 +0100

octavia-tempest-plugin (2.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 27 Feb 2024 09:48:22 +0100

octavia-tempest-plugin (2.4.1-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Oct 2023 08:28:27 +0200

octavia-tempest-plugin (2.4.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Oct 2023 18:48:17 +0200

octavia-tempest-plugin (2.4.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Sep 2023 16:52:41 +0200

octavia-tempest-plugin (2.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 31 Aug 2023 09:14:43 +0200

octavia-tempest-plugin (2.3.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1046467).

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Aug 2023 09:52:21 +0200

octavia-tempest-plugin (2.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 16:03:21 +0200

octavia-tempest-plugin (2.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 10 Mar 2023 15:10:46 +0100

octavia-tempest-plugin (2.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Feb 2023 10:44:18 +0100

octavia-tempest-plugin (2.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Sep 2022 16:29:53 +0200

octavia-tempest-plugin (1.9.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Mar 2022 14:12:14 +0200

octavia-tempest-plugin (1.9.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Mar 2022 17:48:39 +0100

octavia-tempest-plugin (1.8.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:41:45 +0200

octavia-tempest-plugin (1.8.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Sep 2021 11:01:34 +0200

octavia-tempest-plugin (1.8.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Aug 2021 10:51:25 +0200

octavia-tempest-plugin (1.7.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:57:56 +0200

octavia-tempest-plugin (1.7.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Mar 2021 13:51:50 +0200

octavia-tempest-plugin (1.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed verison of openstack-pkg-tools from b-d.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 14:15:52 +0100

octavia-tempest-plugin (1.5.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 15:42:20 +0200

octavia-tempest-plugin (1.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this package, note that httpx 0.14.2 is needed,
    but only 0.12 is available right now.

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Oct 2020 14:48:11 +0200

octavia-tempest-plugin (1.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jun 2020 11:03:34 +0200

octavia-tempest-plugin (1.4.0-1) experimental; urgency=medium

  * Initial packaging (Closes: #958792).

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Apr 2020 17:21:46 +0200
